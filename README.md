# JavaScript
### Libraries
* [LIST OF USEFUL LIBRARIES - Awesome JavaScript](https://github.com/sorrycc/awesome-javascript)
* [LIST OF OPEN SOURCE JAVASCRIPT PROJECTS](https://gauger.io/contrib/#/language/javascript)
* [JavaScript Graphics Library (JSGL)](http://jsgl.org/doku.php)
* [Tabulator](http://tabulator.info/) - The easy to use, fully featured, interactive table JavaScript library
* [Redux](https://redux.js.org/) - A predictable state container for JavaScript apps
* [Cleave.js](https://nosir.github.io/cleave.js/)  - Format your <input/> content when you are typing
* [Micromodal.js](https://micromodal.now.sh/) - modal dialogs in pure JavaScript
* [Pill](https://github.com/rumkin/pill)  - adds dynamic content loading to static sites / [youtube](https://www.youtube.com/watch?v=Q7gRx49JPUs&feature=youtu.be)
* [topbar](https://github.com/buunguyen/topbar) - Site-wide progress indicator / [youtube](https://www.youtube.com/watch?v=Q7gRx49JPUs&feature=youtu.be)
* [ifvisible.js](https://github.com/serkanyersen/ifvisible.js) - Crossbrowser & lightweight way to check if user is looking at the page or interacting with it / [youtube](https://www.youtube.com/watch?v=Q7gRx49JPUs&feature=youtu.be)
* [Wade](https://github.com/kbrsh/wade) - search engine / [youtube](https://www.youtube.com/watch?v=Q7gRx49JPUs&feature=youtu.be)
* [redom](https://github.com/redom/redom) - library for creating user interfaces / [youtube](https://www.youtube.com/watch?v=Q7gRx49JPUs&feature=youtu.be)
* [Javascript-Graphics-Framework](https://github.com/ThomasAn73/OpenSource-Javascript-GraphicsFramework) - framework that abstracts away from canvas2D and WebGL
* [laxxx](https://github.com/alexfoxy/laxxx) - Simple & light weight (3kb minified & zipped) vanilla javascript plugin to create smooth & beautiful animations when you scrolllll! Harness the power of the most intuitive interaction and make your websites come alive!
* [Headroom.js](https://wicky.nillia.ms/headroom.js/) - Give your pages some headroom. Hide your header until you need it.

### Charting libraries
* [D3.js](https://d3js.org/) /open source/ /free/
* [Chart.js](https://www.chartjs.org/) /open source/ /free/
* [JSCharting](https://jscharting.com/) /free for non commercial/
* [Highcharts](https://www.highcharts.com/) /free for non commercial + branded/
* [amCharts](https://www.amcharts.com/) /free/paid/
* [ZingChart](https://www.zingchart.com/) /free + branded/
* [FushionCharts](https://www.fusioncharts.com/) /free + branded/

### Text-editors
* [tiptap](https://github.com/scrumpy/tiptap) - A renderless and extendable rich-text editor for Vue.js

### Three.js
* [three.js](https://threejs.org/)
* [Intro to WebGL with Three.js](http://davidscottlyons.com/threejs-intro/#slide-0)

### Other
* [JavaScript Component Builder](https://sideview.app/)
* [Opal - Ruby to JavaScript source-to-source compiler](http://opalrb.com/)
* [GrapesJS](https://grapesjs.com/) - Free and Open Source Web Builder Framework

### Patterns, Algorithms, Cheat Sheets
* [JavaScript Algorithms and Data Structures](https://github.com/trekhleb/javascript-algorithms)
* [15 JavaScript concepts that every (JavaScript) Programmer must know.](https://medium.com/@madasamy/15-javascript-concepts-that-every-nodejs-programmer-must-to-know-6894f5157cb7)
* [Top 10 JavaScript Patterns Every Developer Like](https://dev.to/shijiezhou/top-10-javascript-patterns-every-developers-like-168p)
* [Array Operations Cheat Sheet](https://devinduct.com/cheatsheet/8/array-operations)

# Portals
* [PL - devszczepaniak.pl](https://devszczepaniak.pl/)

# Articles
* [Understanding Express.js: Creating Your Own Node HTTP Request Router](https://hackernoon.com/understanding-express-js-creating-your-own-node-http-request-router-4190a9b6aad6)
* [React Tutorial: Building and Securing Your First App](https://morioh.com/p/560b4c7153c5/react-tutorial-building-and-securing-your-first-app)
* [How to build a Node.js eCommerce website for free](https://dev.to/amejiarosario/how-to-build-a-node-js-ecommerce-website-for-free-2o97)
* [How to build historical price charts with D3.js](https://medium.freecodecamp.org/how-to-build-historical-price-charts-with-d3-js-72214aaf6ba3)
* [Why you need Prettier in your life if you work in a team](https://itnext.io/why-you-need-prettier-in-your-life-if-you-work-in-a-team-8b0658dc1d89)
* [Building Web based UIs for Terminals using JavaScript](https://medium.com/@atulanand94/building-web-based-uis-for-terminals-using-javascript-60b5eee31213)
* [An introduction to the JAMstack: the architecture of the modern web](https://medium.freecodecamp.org/an-introduction-to-the-jamstack-the-architecture-of-the-modern-web-c4a0d128d9ca)
* [How to show desktop notifications using JavaScript](https://dev.to/attacomsian/how-to-show-desktop-notifications-using-javascript-5aco)
* [Quick JavaScript Tip: the some() method](https://dev.to/mattsparks/quick-javascript-tip-the-some-method-207j)
* [How to Create a Secure Node.js GraphQL API](https://www.toptal.com/graphql/graphql-nodejs-api)
* [Make your site’s pages instant in 1 minute and improve your conversion rate by 1%](https://instant.page/)
* [9 Web Components UI Libraries You Should Know in 2019](https://blog.bitsrc.io/9-web-component-ui-libraries-you-should-know-in-2019-9d4476c3f103)
* [Web Components Tutorial for Beginners (2019)](https://www.robinwieruch.de/web-components-tutorial/)
* [Why are the username and password on two different pages?](https://www.twilio.com/blog/why-username-and-password-on-two-different-pages)
* [FUNCTIONAL JAVASCRIPT: WHAT ARE HIGHER-ORDER FUNCTIONS, AND WHY SHOULD ANYONE CARE?](https://jrsinclair.com/articles/2019/what-is-a-higher-order-function-and-why-should-anyone-care/)
* [Long Live the Virtual DOM](https://github.com/gactjs/gact/blob/master/docs/long-live-the-virtual-dom.md)
* [Build an SMS Event Notifier in 5 Minutes with Airtable, Standard Library and Node.js](https://medium.com/@keithwhor/build-an-sms-event-notifier-in-5-minutes-with-airtable-standard-library-and-node-js-548f9c548a29)
* [Practical Ways to Write Better JavaScript](https://dev.to/taillogs/practical-ways-to-write-better-javascript-26d4)
* [ReactJS / JavaScript Fetch API Cheatsheet](https://blog.codemy.net/javascript-fetch-api-cheatsheet/)
* [VSCode / My VS Code setup - Making the most out of VS Code](https://dev.to/deepu105/my-vs-code-setup-making-the-most-out-of-vs-code-4enl)
* [VSCode / Tips to use VSCode more efficiently](https://dev.to/selrond/tips-to-use-vscode-more-efficiently-3h6p)

# Tools
* [crudpi](https://crudpi.io/) - Create your CRUD API with one click

# Topics
### Dashboards
* [AngularJS Bootstrap / Blur Admin](https://github.com/akveo/blur-admin) / [demo](http://akveo.com/blur-admin-mint/#/dashboard) - MIT license
* [AdminLTE](https://github.com/ColorlibHQ/AdminLTE) - Bootstrap 3, MIT license
* [ModularAdmin](https://github.com/modularcode/modular-admin-html) - Bootstrap 4, MIT license
* [SB Admin](https://startbootstrap.com/templates/sb-admin/) - Bootstrap 4, MIT license
* [StarAdmin](https://github.com/BootstrapDash/StarAdmin-Free-Bootstrap-Admin-Template) - Bootstrap 4, MIT license

### REST API
* [Getting started with designing RESTful APIs](https://hub.packtpub.com/getting-started-with-designing-restful-apis/)

### GraphQL
* [GraphQL Playground](https://www.graphqlbin.com/v2/6RQ6TM)
* [Core Concepts](https://www.howtographql.com/basics/2-core-concepts/)
* [Graphene](https://graphene-python.org/) - Graphene-Python is a library for building GraphQL APIs in Python
* [GraphQl Editor](https://github.com/graphql-editor/graphql-editor) - Visual Editor for GraphQL
* [GraphQL Crash Course (in 10 pics!)](https://dev.to/hexrcs/graphql-crash-course-in-10-pics-3b04)

##### GraphQL + PostgreSQL
* [Graphile](https://www.graphile.org/) - Extensible high-performance automatic GraphQL API for PostgreSQL
* [Graphile github](https://github.com/graphile)
* [Hasura](https://hasura.io/) - Instant Realtime GraphQL on Postgres
* [Hasura graphql-engine github](https://github.com/hasura/graphql-engine)

### Electron
* [Boilerplate application for Electron runtime](https://github.com/bradstewart/electron-boilerplate-vue)
* [Electron as GUI of Python Applications (Updated)](https://www.fyears.org/2017/02/electron-as-gui-of-python-apps-updated.html)

### Other
* [Cookiechill](https://cookiechill.com/) - Cookie generator. Resolve cookie conflicts and download your Cookie Policy
* [Canvas-txt](http://canvas-txt.geongeorge.com/) - A library to print multiline text on HTML5 canvas with better line breaks and alignments
* [Sizzy](https://sizzy.co/) - Web browser for webdesigners and webmasters
* [urlpages](https://github.com/jstrieb/urlpages) - Create and view web pages stored entirely in the URL
* [HTTP Security Headers - A Complete Guide](https://nullsweep.com/http-security-headers-a-complete-guide/)
* [Smashtest](https://smashtest.io/) - Smashtest is a language for rapidly describing and deploying test cases.

### Wordpress
* [SiteOrigin](https://siteorigin.com/) - Plugin for WordPress to generate pages via drag and drop

### Authentication
* [Introduction to JSON Web Tokens](https://jwt.io/introduction/)

### Other
* [Update all the Node dependencies to their latest version](https://flaviocopes.com/update-npm-dependencies/)

# HTML & CSS
### Portals
* [CSS Tricks](https://css-tricks.com)
* [13 CSS Grid tips and tricks which you should know as a web developer](https://dev.to/duomly/css-grid-cheatsheet-what-is-the-css-grid-and-how-to-use-it-2gp8)

### Articles
* [Things nobody ever taught me about CSS.](https://medium.com/@devdevcharlie/things-nobody-ever-taught-me-about-css-5d16be8d5d0e)
* [A list of amazing things that CSS can do!](https://dev.to/ananyaneogi/css-can-do-that-18g7)
* [An Introduction to the Reduced Motion Media Query](https://css-tricks.com/introduction-reduced-motion-media-query/)
* [CSS-Only Method to Track Mouse Movements](https://www.bleepingcomputer.com/news/security/researcher-finds-css-only-method-to-track-mouse-movements/) 
* [The CSS background-image property as an anti-pattern](https://nystudio107.com/blog/the-css-background-image-property-as-an-anti-pattern)
* [Units in CSS (em, rem, pt, px, vw, vh, vmin, vmax, ex, ch, ...)](https://dev.to/fullstack_to/units-in-css-em-rem-pt-px-vw-vh-vmin-vmax-ex-ch-53l0)
* [A friendly web development tutorial for complete beginners](https://internetingishard.com/html-and-css/)
* [The Beginner’s Guide to Chart.js](https://www.stanleyulili.com/javascript/beginner-guide-to-chartjs/)

# Bootstrap
### Plugins
* [bootstrap-input-spinner](https://shaack.com/projekte/bootstrap-input-spinner/) - A Bootstrap 4 / jQuery plugin to create input spinner elements for number input, by shaack.com engineering.
* [bootstrap-popover-picker](https://github.com/farbelous/bootstrap-popover-picker) - Generic Bootstrap plugin template for building selector components with popovers. 
 
# Vue.js
### Various
* [Awesome Vue.js](https://github.com/vuejs/awesome-vue#appswebsites) - A curated list of awesome things related to Vue.js
* [Vue i18n](https://github.com/antfu/vue-i18n-ally) - plugin for VSCode 
* [Directus](https://directus.io/) - Future-Proof Headless CMS
* [Stock trader simulation for Vue.js Udemy coarse](https://vuejsexamples.com/stock-trader-simulation-for-vue-js-udemy-coarse/)
* [Your next email template builder](https://mysigmail.com/card/)
 
### Tutorials
* [Getting Started with Vue - An Overview and Walkthrough Tutorial](https://www.taniarascia.com/getting-started-with-vue/)
* [Vue and TypeScript tutorial](https://sobolevn.me/2019/06/really-typing-vue?utm_source=Vue.js+Developers&utm_campaign=b795597181-EMAIL_CAMPAIGN_2019_07_01_05_30_COPY_01&utm_medium=email&utm_term=0_ae2f1465e2-b795597181-191474633)
* [List Rendering with v-for in Vue JS ❖ Beginner Tutorial](https://www.youtube.com/watch?v=6cBeU6bcsSI)
 
### Templates
* [CoPilot](https://github.com/misterGF/CoPilot) - Responsive Bootstrap 3 Admin Template based on AdminLTE with vue.js. [demo](https://copilot.mistergf.io/)
* [VUESTIC](https://github.com/epicmaxco/vuestic-admin) - open source admin template. [demo](https://vuestic.epicmax.co/#/admin/dashboard))
* [Vue Element Admin](https://github.com/PanJiaChen/vue-element-admin) - open source admin template. [demo](https://panjiachen.github.io/vue-element-admin/#/dashboard)

### Components
* <b>[Ant Design Components](https://vue.ant.design/docs/vue/introduce/)</b>
* <b>[Element](https://element.eleme.io/#/en-US)</b> - a Vue 2.0 based component library for developers, designers and product managers
* [Virtual Keyboard](https://github.com/hodgef/simple-keyboard) - Javascript Virtual Keyboard - Customizable, responsive and lightweight 
* [JSON UI Editor](https://github.com/yourtion/vue-json-ui-editor)
* [DatePicker](https://iviewui.com/components/date-picker-en)
* [ARTICLE - 11 Vue.js UI Component Libraries you Should Know in 2019](https://blog.bitsrc.io/11-vue-js-component-libraries-you-should-know-in-2018-3d35ad0ae37f)
 
### Topics
* [A deep dive in the Vue.js source code](https://itnext.io/a-deep-dive-in-the-vue-js-source-code-4601a3f5584?source=friends_link&sk=b19fa84aa33b0c6e1a9f24af2460faa8&gi=13bcea87b503)
* [Vue.js 3: Future-Oriented Programming](https://blog.bitsrc.io/vue-js-3-future-oriented-programming-54dee797988b)
* [GRAPHQL - ADDING GRAPHQL TO YOUR VUEJS APPLICATION](https://thecodingmachine.io/adding-graphql-to-your-vuejs-application)
* [VUEX - Understanding data flow in Vuex](https://jerickson.net/understanding-data-flow-in-vuex/)
* [TESTING - Vue Test Utils](https://vue-test-utils.vuejs.org/)
* [TESTING - The Best Cross-Browser Testing Tools](https://dev.to/razgandeanu/the-best-cross-browser-testing-tools-4hd6)
* [MODAL DIALOGS - Beginner’s Guide to Vue Modals](https://www.genuitec.com/beginners-guide-to-vue-modals/?utm_source=reddit&utm_medium=organic&utm_campaign=codemix)
* [DIRECTIVES - v-outer-html](https://www.npmjs.com/package/v-outer-html)
* [AUTHENTICATION - Token Authentication with Rails, Vue, GraphQL and Devise](https://engineering.doximity.com/articles/token-authentication-with-rails-vue-graphql-and-devise)
* [AUTHENTICATION - The Lazy Developer's Guide to Authentication with Vue.js](https://developer.okta.com/blog/2017/09/14/lazy-developers-guide-to-auth-with-vue)

# Design tools & Resources
* [Web Dev Resources](https://webdevresources.info/)
* [CollorKitty](https://colorkitty.com/) - photo based palette generator 
* [Flexer App](https://flexerapp.com/) - Design UI applications and generate code for different web and mobile frameworks.
* [Blobmaker](https://www.blobmaker.app/)
* [Gradient Magic](https://www.gradientmagic.com/) - A Gallery of Fantastic and Unique CSS Gradients

### Icons
* [Themify Icons](https://themify.me/themify-icons)
* [Font Awesome](https://fontawesome.com/icons?from=io)
* <b>[Line Awesome](https://iconos8.es/line-awesome)</b>
* <b>[Feather icons](https://feathericons.com/)</b>
* [ICONO](https://saeedalipoor.github.io/icono/)
* [Devicons](http://vorillaz.github.io/devicons/#/main)
* [Stackicons](http://stackicons.com/)
* [Genericons](http://genericons.com/)
* [Material Design Icons](https://materialdesignicons.com/)
* [Octicons](https://octicons.github.com/)
* [Ionicons](https://ionicons.com/)

### Fonts
* [30 Great Free Fonts for 2019](https://medium.muz.li/30-great-free-fonts-for-2019-14814a17e91a?gi=8448a29e58eb)

### Images
* [Artworks](https://www.artic.edu/collection?is_public_domain=1)

### Tips
* [8 Tips for Dark Theme Design](https://uxplanet.org/8-tips-for-dark-theme-design-8dfc2f8f7ab6)
* [Checkbox vs Toggle Switch](https://uxplanet.org/checkbox-vs-toggle-switch-7fc6e83f10b8)

# Various
* [Erlyweb](https://en.wikibooks.org/wiki/ErlyWeb) - an open-source web application framework written in Erlang and released under the GPL
* [HTML Emails 101 For Web Developers](https://medium.com/@andrewlaurentiu/html-emails-4de656a6b7ef)